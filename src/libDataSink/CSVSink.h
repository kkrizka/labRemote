#ifndef CSVSINK_H
#define CSVSINK_H

#include <fstream>
#include <unordered_map>
#include <vector>

#include "IDataSink.h"

//! \brief Data sink that saves to CSV files with comma delimiter
/**
 * When a data point is recorded, the following are saved in this order,
 * separated by a comma:
 * - timestamp (under column heading time)
 * - tags
 * - fields
 *
 * The header (time, tag names, and field names) are written to a file when a
 * file is first created.
 */
class CSVSink : public IDataSink {
 public:
    CSVSink(const std::string& name);

    //! \brief Configure file sink based on JSON object
    /**
     * Valid keys:
     *  - directory: output directory where files are saved (required)
     *
     * \param config JSON configuration
     */
    virtual void setConfiguration(const nlohmann::json& config);

    //! Initialize sink
    /**
     * Attemps to create `directory`, if it does not already exist.
     */
    virtual void init();

    virtual void setTag(const std::string& name, const std::string& value);
    virtual void setTag(const std::string& name, int8_t value);
    virtual void setTag(const std::string& name, int32_t value);
    virtual void setTag(const std::string& name, int64_t value);
    virtual void setTag(const std::string& name, double value);

    virtual void startMeasurement(
        const std::string& measurement,
        std::chrono::time_point<std::chrono::system_clock> time);
    virtual void setField(const std::string& name, const std::string& value);
    virtual void setField(const std::string& name, int8_t value);
    virtual void setField(const std::string& name, int32_t value);
    virtual void setField(const std::string& name, int64_t value);
    virtual void setField(const std::string& name, double value);

    //! \brief Save data point to CSV
    /**
     * Saves the fields in the current data points in csv file. If the header
     * needs to be written to file, it is also done here.
     * The timestamp will be uploaded with milliseconds precision.
     */
    virtual void recordPoint();
    virtual void endMeasurement();

 private:
    //! \brief Check if string contains CSV character
    /**
     * Check if the string contains a comma or double quote.
     *
     * If the string contains a comma, return the string in double-quotes.
     * If the string contains quotes and a comma, the quotation mark will be
     * escaped.
     *
     * \param name string to be check.
     *
     * return encoded string if string contains a comma.
     * else return original name.
     */
    std::string checkCSVEncoding(const std::string& name);

    //! \brief Checks whether a tag can be added
    /**
     * If tag is not defined, then it is added to the list.
     *
     * Marks tags as dirty.
     *
     * Error checking is performed. An exception is thrown if
     *  - Currently performing a measurement
     *
     * \param name Field name
     */
    void checkTag(const std::string& name);

    //! \brief Adds a new field column if it already does not exists.
    /**
     * If field is already defined, nothing is done.
     *
     * Error checking is performed. An exception is thrown if
     *  - called after the first `recordPoint` in a measurement
     *
     * \param name Field name
     */
    void addField(const std::string& name);

    //! \brief write the header
    /**
     * write the header in the format.
     *  Tag1 Tag 2 FIELD1 FIELD2 FIELD3
     *
     * \param ofile file to save the measurement
     */
    void printHeader(std::ofstream& ofile);

    //
    // Directory where files are saved
    std::string m_directory;

    //
    // State

    // timestamp
    unsigned long long m_timestamp;

    // Output CSV file
    std::ofstream m_ofile;

    // Name of the current measurement (valid onyl if performing one)
    std::string m_measurement;

    // Currently performing a measurement
    bool m_inMeasurement = false;

    // Whether or not the field header has been written
    bool m_header_written;

    //
    // Data store for fields
    bool m_finalSchema = false;  // The measurement schema is locked
    std::vector<std::string> m_fields;
    std::unordered_map<std::string, std::string> m_fieldsString;
    std::unordered_map<std::string, int8_t> m_fieldsInt8;
    std::unordered_map<std::string, int32_t> m_fieldsInt32;
    std::unordered_map<std::string, int64_t> m_fieldsInt64;
    std::unordered_map<std::string, double> m_fieldsDouble;

    //
    // Data store for tags
    std::vector<std::string> m_tags;
    std::unordered_map<std::string, std::string> m_tagsString;
    std::unordered_map<std::string, int8_t> m_tagsInt8;
    std::unordered_map<std::string, int32_t> m_tagsInt32;
    std::unordered_map<std::string, int64_t> m_tagsInt64;
    std::unordered_map<std::string, double> m_tagsDouble;
};

#endif  // CSVSINK_H
