add_library(DataSink SHARED)
target_sources(DataSink
  PRIVATE
  IDataSink.cpp
  CombinedSink.cpp
  ConsoleSink.cpp
  CSVSink.cpp
  DataSinkRegistry.cpp
  )

target_link_libraries(DataSink PRIVATE Utils)
target_link_libraries(DataSink PUBLIC ${JSON_LIBS} nlohmann_json_schema_validator)
target_include_directories(DataSink PUBLIC ${CMAKE_CURRENT_SOURCE_DIR})

# Add optional influxdb datasink
if( ${INFLUXDBCPP_FOUND} )
  target_sources(DataSink
    PRIVATE
    InfluxDBSink.cpp
    )
  target_include_directories (DataSink PRIVATE ${INFLUXDBCPP_INCLUDE_DIR})
else()
  message(STATUS "Skipping InfluxDBSink due to missing influxdb-cpp")
endif()

set_target_properties(DataSink PROPERTIES VERSION ${labRemote_VERSION_MAJOR}.${labRemote_VERSION_MINOR})
install(TARGETS DataSink)
