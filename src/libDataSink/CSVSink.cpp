#include "CSVSink.h"

#include <algorithm>
#include <iostream>
#include <sstream>

#include "Logger.h"

#if defined(__unix__) || (defined(__APPLE__) && defined(__MACH__))
#include <sys/stat.h>
#endif

#include "DataSinkRegistry.h"
REGISTER_DATASINK(CSVSink)

CSVSink::CSVSink(const std::string& name) : IDataSink(name) {}

void CSVSink::setConfiguration(const nlohmann::json& config) {
    // check if directory is specified
    auto iter = config.find("directory");
    if (iter == config.end()) {
        throw std::runtime_error("Need to specify directory for CSV sink");
    }

    for (const auto& kv : config.items()) {
        if (kv.key() == "directory") {
            m_directory = kv.value();
        }
    }
}

void CSVSink::init() {
    //
    // Create the output directory
#if defined(__unix__) || (defined(__APPLE__) && defined(__MACH__))
    std::stringstream ss(m_directory);
    std::string basename;
    std::string path;

    struct stat sb;
    while (std::getline(ss, basename, '/')) {
        path += (basename + "/");

        if (stat(path.c_str(), &sb) == 0) continue;  // Already exists!

        if (mkdir(path.c_str(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH) != 0)
            throw std::runtime_error("Unable to create directory " + path +
                                     ": " + std::strerror(errno));
    }
#else
    logger(logWARNING) << "Check that " << m_directory << " exists!";
#endif
}

void CSVSink::setTag(const std::string& name, const std::string& value) {
    std::string newTag = checkCSVEncoding(name);
    checkTag(newTag);
    m_tagsString[newTag] = checkCSVEncoding(value);
}

void CSVSink::setTag(const std::string& name, int8_t value) {
    std::string newTag = checkCSVEncoding(name);
    checkTag(newTag);
    m_tagsInt8[newTag] = value;
}

void CSVSink::setTag(const std::string& name, int32_t value) {
    std::string newTag = checkCSVEncoding(name);
    checkTag(newTag);
    m_tagsInt32[newTag] = value;
}

void CSVSink::setTag(const std::string& name, int64_t value) {
    std::string newTag = checkCSVEncoding(name);
    checkTag(newTag);
    m_tagsInt64[newTag] = value;
}

void CSVSink::setTag(const std::string& name, double value) {
    std::string newTag = checkCSVEncoding(name);
    checkTag(newTag);
    m_tagsDouble[newTag] = value;
}

void CSVSink::startMeasurement(
    const std::string& measurement,
    std::chrono::time_point<std::chrono::system_clock> time) {
    // Clean-up last measurement
    m_fields.clear();
    m_fieldsString.clear();
    m_fieldsInt8.clear();
    m_fieldsInt32.clear();
    m_fieldsInt64.clear();
    m_fieldsDouble.clear();

    // State
    m_finalSchema = false;
    m_inMeasurement = true;

    m_measurement = measurement;

    // Convert the timestamp to ms
    m_timestamp = std::chrono::duration_cast<std::chrono::milliseconds>(
                      time.time_since_epoch())
                      .count();

    std::string fileName = m_directory + "/" + m_measurement + ".csv";

    // Check if this file already exists, as we are appending to it and we
    // want to know (later) if we need to writ the CSV format header (since
    // we assume that the already existing data format (the column fields) are
    // the same (!!!!)
    std::ifstream tmp_file(
        fileName, std::ios_base::out);  // open for read will not be good if the
                                        // file does not exist
    bool file_already_exists = tmp_file.good();
    m_header_written =
        file_already_exists;  // here is our assumption on the data format and
                              // appending to file
    tmp_file.close();

    // Open CSV file, appending to it
    m_ofile.open(fileName, std::ios_base::app);
    if (!(m_ofile.is_open()))
        throw std::runtime_error("Could not open file: " + fileName);
}

void CSVSink::setField(const std::string& name, const std::string& value) {
    std::string newField = checkCSVEncoding(name);
    addField(newField);
    m_fieldsString[newField] = checkCSVEncoding(value);
}

void CSVSink::setField(const std::string& name, int8_t value) {
    std::string newField = checkCSVEncoding(name);
    addField(newField);
    m_fieldsInt8[newField] = value;
}

void CSVSink::setField(const std::string& name, int32_t value) {
    std::string newField = checkCSVEncoding(name);
    addField(newField);
    m_fieldsInt32[newField] = value;
}

void CSVSink::setField(const std::string& name, int64_t value) {
    std::string newField = checkCSVEncoding(name);
    addField(newField);
    m_fieldsInt64[newField] = value;
}

void CSVSink::setField(const std::string& name, double value) {
    std::string newField = checkCSVEncoding(name);
    addField(newField);
    m_fieldsDouble[newField] = value;
}

void CSVSink::recordPoint() {
    // Print the header
    if (!m_header_written) {
        printHeader(m_ofile);
    }

    // Print timestamp
    m_ofile << m_timestamp;

    //
    // Print the tags
    for (const std::string& tag : m_tags) {
        m_ofile << ",";

        if (m_tagsString.count(tag) > 0) m_ofile << m_tagsString[tag];

        if (m_tagsInt8.count(tag) > 0) m_ofile << m_tagsInt8[tag];

        if (m_tagsInt32.count(tag) > 0) m_ofile << m_tagsInt32[tag];

        if (m_tagsInt64.count(tag) > 0) m_ofile << m_tagsInt64[tag];

        if (m_tagsDouble.count(tag) > 0) m_ofile << m_tagsDouble[tag];
    }

    //
    // Print the fields
    for (const std::string& field : m_fields) {
        m_ofile << ",";

        if (m_fieldsString.count(field) > 0) m_ofile << m_fieldsString[field];

        if (m_fieldsInt8.count(field) > 0) m_ofile << m_fieldsInt8[field];

        if (m_fieldsInt32.count(field) > 0) m_ofile << m_fieldsInt32[field];

        if (m_fieldsInt64.count(field) > 0) m_ofile << m_fieldsInt64[field];

        if (m_fieldsDouble.count(field) > 0) m_ofile << m_fieldsDouble[field];
    }
    m_ofile << std::endl;

    // flush the output so that it is written to disk immediately
    m_ofile.flush();

    m_finalSchema = true;
}

void CSVSink::endMeasurement() {
    m_inMeasurement = false;
    m_ofile.close();
}

std::string CSVSink::checkCSVEncoding(const std::string& name) {
    std::string newstr = name;

    // check if the name contains a comma
    if (name.find(',') != std::string::npos) {
        size_t start_pos = 0;
        static const std::string sold = "\"";
        static const std::string snew = "\"\"";

        // check if string has quotation mark, if so replace by double quotation
        // marks
        while ((start_pos = newstr.find(sold, start_pos)) !=
               std::string::npos) {
            newstr.replace(start_pos, sold.length(), snew);
            start_pos += snew.length();
        }

        // enclose the string in double quotes
        newstr = "\"" + newstr + "\"";
    }
    return newstr;
}

void CSVSink::checkTag(const std::string& name) {
    if (checkReserved(name))
        throw std::runtime_error("Tag name \"" + name + "\" is reserved.");

    if (std::find(m_tags.begin(), m_tags.end(), name) == m_tags.end()) {
        if (m_inMeasurement)
            throw std::runtime_error(
                "CSVSink: Cannot change tag during measurement.");

        m_tags.push_back(name);
    }
}

void CSVSink::addField(const std::string& name) {
    if (checkReserved(name))
        throw std::runtime_error("Tag name \"" + name + "\" is reserved.");

    if (std::find(m_fields.begin(), m_fields.end(), name) == m_fields.end()) {
        if (m_finalSchema)
            throw std::runtime_error(
                "CSVSink: Cannot add new fields after the first recordPoint.");

        m_fields.push_back(name);
    }
}

void CSVSink::printHeader(std::ofstream& ofile) {
    ofile << "time";

    // Print out the tags
    for (const std::string& tag : m_tags) {
        ofile << "," << tag;
    }

    // Print field names
    for (const std::string& field : m_fields) {
        ofile << "," << field;
    }
    ofile << std::endl;
    m_header_written = true;
}
