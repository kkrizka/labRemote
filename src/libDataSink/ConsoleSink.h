#ifndef CONSOLESINK_H
#define CONSOLESINK_H

#include <unordered_map>
#include <vector>

#include "IDataSink.h"

//! \brief Data sink that prints to the console
/**
 * When a data point is recorded, all fields are printed to the
 * standard output as a pretty table. The format is as follows:
 *
 * ```
 *  Tag1: value
 *  ...
 *  Tagn: value
 *  FIELD1 FIELD2 FIELD3
 *  VAL1   VAL2   VAL3
 * ```
 *
 * For fields, the columns are taken to have 10 characters by default.
 *
 * The header (tags and the column names) are printed when any of the following
 * happens:
 *  - The name of the measurement changes.
 *  - A tag value changes.
 *  - The set of fields changes.
 *
 * The header is not printed just because a previous measurement has been
 * stopped. If the field definitions and tags are the same, the previous header
 * still applies.
 */
class ConsoleSink : public IDataSink {
 public:
    ConsoleSink(const std::string& name);

    //! \brief Reads the output configuration from a JSON object.
    /**
     * See an example in `src/configs/input-hw.json`.
     *
     * Valid keys:
     *  - `column_witdth`: Width of each column in characters
     *
     * * \param config JSON configuration
     */
    virtual void setConfiguration(const nlohmann::json& config);
    virtual void init();

    virtual void setTag(const std::string& name, const std::string& value);
    virtual void setTag(const std::string& name, int8_t value);
    virtual void setTag(const std::string& name, int32_t value);
    virtual void setTag(const std::string& name, int64_t value);
    virtual void setTag(const std::string& name, double value);

    virtual void startMeasurement(
        const std::string& measurement,
        std::chrono::time_point<std::chrono::system_clock> time);
    virtual void setField(const std::string& name, const std::string& value);
    virtual void setField(const std::string& name, int8_t value);
    virtual void setField(const std::string& name, int32_t value);
    virtual void setField(const std::string& name, int64_t value);
    virtual void setField(const std::string& name, double value);

    //! \brief Print data point
    /**
     * Prints the fields in the current data points. If the header
     * needs to be reprinted, it is also done here.
     */
    virtual void recordPoint();
    virtual void endMeasurement();

 private:
    //! \brief Checks whether a tag can be added
    /**
     * If tag is not defined, then it is added to the list.
     *
     * Marks tags as dirty.
     *
     * Error checking is performed. An exception is thrown if
     *  - Currently performing a measurement
     *
     * \param name Field name
     */
    void checkTag(const std::string& name);

    //! \brief Adds a new field column if it already does not exists.
    /**
     * If field is already defined, nothing is done.
     *
     * Error checking is performed. An exception is thrown if
     *  - called after the first `recordPoint` in a measurement
     *
     * \param name Field name
     */
    void addField(const std::string& name);

    //! \brief Print the header
    /**
     * Prints the header in the format.
     *  Tag1: value
     *  ...
     *  Tagn: value
     *  FIELD1 FIELD2 FIELD3
     */
    void printHeader();

    //
    // The last state (before endMeasurement)

    // Name of the last measurement
    std::string m_lastMeasurement;

    // The last columns
    std::vector<std::string> m_lastFields;

    //
    // State

    // Name of the current measurement (valid onyl if performing one)
    std::string m_currMeasurement;

    // Currently performing a measurement
    bool m_inMeasurement = false;

    // Field width (number of characters allocated for each column entry)
    int m_col_width = 10;

    //
    // Data store for fields
    bool m_finalSchema = false;  // The measurement schema is locked
    std::vector<std::string> m_fields;
    std::unordered_map<std::string, std::string> m_fieldsString;
    std::unordered_map<std::string, int8_t> m_fieldsInt8;
    std::unordered_map<std::string, int32_t> m_fieldsInt32;
    std::unordered_map<std::string, int64_t> m_fieldsInt64;
    std::unordered_map<std::string, double> m_fieldsDouble;

    //
    // Data store for tags
    bool m_dirty = true;  // Tag values have changed, reprint
    std::vector<std::string> m_tags;
    std::unordered_map<std::string, std::string> m_tagsString;
    std::unordered_map<std::string, int8_t> m_tagsInt8;
    std::unordered_map<std::string, int32_t> m_tagsInt32;
    std::unordered_map<std::string, int64_t> m_tagsInt64;
    std::unordered_map<std::string, double> m_tagsDouble;
};

#endif  // CONSOLESINK_H
