#include "DeviceComRegistry.h"

#include <algorithm>
#include <iostream>

namespace DeviceComRegistry {

typedef std::map<std::string, std::vector<std::string>> DevComRegistry_t;

static DevComRegistry_t& registry() {
    static DevComRegistry_t instance;
    return instance;
}

bool registerDeviceCom(const std::string& device_name,
                       const std::string& device_type) {
    DevComRegistry_t reg = registry();
    if (reg.find(device_type) != reg.end()) {
        std::vector<std::string> devices = reg.at(device_type);
        if (std::find(devices.begin(), devices.end(), device_name) ==
            devices.end()) {
            registry().at(device_type).push_back(device_name);
        }
    } else {
        registry()[device_type] = {device_name};
    }
    return true;
}

std::vector<std::string> listDeviceTypes() {
    std::vector<std::string> types;
    for (auto type : registry()) {
        types.push_back(type.first);
    }
    return types;
}

std::vector<std::string> listDevices() {
    std::vector<std::string> complete_device_list;
    for (auto type : registry()) {
        std::vector<std::string> devices = type.second;
        for (auto device : devices) {
            complete_device_list.push_back(device);
        }
    }
    return complete_device_list;
}

std::map<std::string, std::vector<std::string>> listDevCom() {
    std::map<std::string, std::vector<std::string>> out;
    for (auto type : registry()) {
        out[type.first] = type.second;
    }
    return out;
}

}  // namespace DeviceComRegistry
