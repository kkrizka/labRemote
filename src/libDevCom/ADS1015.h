#ifndef DEVCOM_ADS1015_H
#define DEVCOM_ADS1015_H

// std/stl
#include <map>
#include <memory>  // shared_ptr
#include <vector>

// labRemote
#include "ADCDevice.h"
class I2CCom;

// clang-format off
/** \brief ADS1015: Low-power, I2C compatible 4-channel 12-bit ADC
 *
 * The `ADS1015` class provides a driver for the Texas Instruments low-power,
 * I2C compatible 4-channel 12-bit ADC. Support for single-shot ADC conversions
 * is supported across all four channels in single-ended measurement mode.
 *
 * [Datasheet](https://www.ti.com/lit/ds/symlink/ads1015.pdf).
 *
 * Support for differential measurements are supported in the following
 * configuration only:
 *    - Across analog inputs 0 and 1: analog input 0 as + and analog input 1 as -
 *    - Across analog inputs 2 and 3: analog input 2 as + and analog input 3 as -
 *
 * Changing the channel mode from single-ended to differential mode is done via
 * the `ADS1015::ChannelMode` enumeration. By default an `ADS1015` instance
 * will be configured for single-ended measurements.
 * Typical use-case for single-ended measurements are as follows:
 *
 *    auto com = ...; // get I2CCom instance
 *    auto adc = std::make_shared<ADS1015>(com);
 *    adc->read();
 *
 * And typical use-cases for differential measurements are as follows:
 *
 *    auto com = ...; // get I2CCom instance
 *    auto adc = std::make_shared<ADS1015>(com);
 *    adc->setChannelMode(ADS1015::ChannelMode::Differential);
 *    adc->read();
 *
 * Adjusting the programmable gain amplifier (PGA) of the ADS1015 device is
 * achieved via either the `ADS1015::setGain` or `ADS1015::setFullScaleRange`
 * methods. The two methods do the same thing, so there is only ever the need to
 * call one of them. Each configuration of the PGA has an associated fullscale
 * range (and LSB size), as described in Table 1 of the
 * [datasheet](https://www.ti.com/lit/ds/symlink/ads1015.pdf). Changing the
 * fullscale range is done via the `ADS1015::FullScaleRange` enumeration and
 * changing the gain is done via the `ADS1015::Gain` enumeration:
 *
 *     adc->setFullScaleRange(ADS1015::FSR_0_512);
 *
 * which is equivalent to setting the corresponding gain value:
 *
 *     adc->setGain(ADS1015::Gain::xEIGHT);
 *
 * The default values for the gain and fullscale range of an instance of
 * `ADS1015` are those of the default power-up state of the device: a gain of
 * `ADS1015::Gain::XTWO`, corresponding to a fullscale range of 2.048 Volts
 * (`ADS1015::FullScaleRange::FSR_2_048`).
 *
 */
// clang-format on
class ADS1015 : public ADCDevice {
 public:
    //! \brief Allowed convertible analog full scale ranges associated with each
    //! programmable gain
    enum class FullScaleRange {
        FSR_6_144,
        FSR_4_096,
        FSR_2_048,
        FSR_1_024,
        FSR_0_512,
        FSR_0_256
    };

    //! \brief Allowed configurations for the programmable gain amplifier
    enum class Gain { xTWOTHIRDS, xONE, xTWO, xFOUR, xEIGHT, xSIXTEEN };

    //! \brief Allowed measurement modes: either differential or single-ended
    enum class ChannelMode { SingleEnded, Differential };

    // clang-format off
    //! \brief ADS1015 constructor
    /**
     * \param com I2C communication device used for communication with the ADS1015
     */
    // clang-format on
    ADS1015(std::shared_ptr<I2CCom> com);

    //! \brief Default destructor
    virtual ~ADS1015() = default;

    //! \brief Perform an ADC conversion on the default ADC channel
    virtual int32_t readCount();

    //! \brief Perform an ADC conversion on a specified ADC channel
    /**
     * \param channel The ADC channel to measure from
     */
    virtual int32_t readCount(uint8_t channel);

    //! \brief Perform ADC conversions on a set of ADC channels
    /**
     * \param channels The set of ADC channels to measure from
     * \param data The array to hold the set of measurements associated with the
     * measurement channels
     */
    virtual void readCount(const std::vector<uint8_t>& channels,
                           std::vector<int32_t>& data);

    // clang-format off
    //! \brief Set the gain
    /**
     * \param gain A value of ADS1015::Gain
     *
     * The ADS1015 has a programmable gain amplifier at the input stage,
     * applied to the analog signals prior to the digitization stage.
     * Depending on the gain setting, the fullscale range of the ADC
     * changes. The fullscale range is the maximum analog value that saturates
     * the digital output of the ADC.
     *
     * This method internally converts the `gain` value to the ADS1015 fullscale
     * range associated with the gain setting and then calls
     * `ADS1015::setFullScaleRange`.
     *
     * WARNING: This method internally resets the calibration used by this ADCDevice instance.
     */
    // clang-format on
    void setGain(Gain gain);

    //! \brief Get the current gain
    Gain getGain() const { return gainFromFSR(m_fullscale_range); }

    // clang-format off
    //! \brief Set the ADS1015 fullscale range
    /**
     * \param range The value of the fullscale range. Must be associated with a
     * valid ADS1015::FullScaleRange.
     *
     * WARNING: This method internally resets the calibration used by this ADCDevice instance.
     */
    // clang-format on
    void setFullScaleRange(FullScaleRange range);

    //! \brief Get the current fullscale range
    FullScaleRange getFullScaleRange() const { return m_fullscale_range; }

    //! \brief Set the channel mode
    void setChannelMode(ChannelMode mode);

    //! \brief Get the current channel mode
    ChannelMode getChannelMode() const { return m_channelMode; }

 private:
    std::shared_ptr<I2CCom> m_com;

    enum class Register : uint16_t {
        CONVERT = 0x00,
        CONFIG = 0x01,
        LOWTHRESH = 0x02,
        HITHRESH = 0x03
    };

    enum class Config : uint16_t {
        OS_NO = 0x8000,
        OS_SINGLE = 0x8000,
        OS_READY = 0x0000,
        OS_NOTREADY = 0x8000,
        MODE_CONT = 0x0000,
        MODE_SINGLE = 0x0100,
        MUX_SINGLE_0 = 0x4000,
        MUX_SINGLE_1 = 0x5000,
        MUX_SINGLE_2 = 0x6000,
        MUX_SINGLE_3 = 0x7000,
        MUX_DIFF_P0_N1 = 0x0000,
        MUX_DIFF_P0_N3 = 0x1000,
        MUX_DIFF_P1_N3 = 0x2000,
        MUX_DIFF_P2_N3 = 0x3000,
        RATE_128HZ = 0x0000,
        RATE_250HZ = 0x0020,
        RATE_490HZ = 0x0040,
        RATE_920HZ = 0x0060,
        RATE_1600HZ = 0x0080,
        RATE_2400HZ = 0x00A0,
        RATE_3300HZ = 0x00C0,
        PGA_MASK = 0X0E00,
        PGA_TWOTHIRDS = 0X0000  // +/- 6.144v
        ,
        PGA_1 = 0X0200  // +/- 4.096v
        ,
        PGA_2 = 0X0400  // +/- 2.048v (default)
        ,
        PGA_4 = 0X0600  // +/- 1.024v
        ,
        PGA_8 = 0X0800  // +/- 0.512v
        ,
        PGA_16 = 0X0A00  // +/- 0.256v
        ,
        CMODE_TRAD = 0x0000  // Traditional comparator with hysteresis (default)
        ,
        CMODE_WINDOW = 0x0010  // Window comparator
        ,
        CPOL_ACTVLOW = 0x0000  // ALERT/RDY pin is low when active (default)
        ,
        CPOL_ACTVHI = 0x0008  // ALERT/RDY pin is high when active
        ,
        CLAT_NONLAT = 0x0000  // Non-latching comparator (default)
        ,
        CLAT_LATCH = 0x0004  // Latching comparator
        ,
        CQUE_1CONV = 0x0000  // Assert ALERT/RDY after one conversions
        ,
        CQUE_2CONV = 0x0001  // Assert ALERT/RDY after two conversions
        ,
        CQUE_4CONV = 0x0002  // Assert ALERT/RDY after four conversions
        ,
        CQUE_NONE = 0x0003  // Disable the comparator and put ALERT/RDY
                            // in high state (default)
    };

    // return the ADS1015::Gain associated with ADS1015::FullScaleRange
    Gain gainFromFSR(FullScaleRange range) const;

    // map to the actual gain value associated with ADS1015::FullScaleRange enum
    // element
    static const std::map<FullScaleRange, double> fsrToValMap;

    // map between programmable gain configuration and ADS1015 full scale range
    // in volts
    static const std::map<Gain, FullScaleRange> gainToFSRMap;

    // map between programmable gain configuration and ADS1015 configuration
    // register values
    static const std::map<Gain, Config> gainToConfigMap;

    // Note that for ADS1015, single-ended mode has 11-bit resolution (max val =
    // 0x7ff). Only when performing differential measurements do you get the
    // full 12-bit resolution with the sign bit taking the 12th bit.
    static const uint32_t m_maxValue;
    FullScaleRange m_fullscale_range = FullScaleRange::FSR_2_048;  // volts
    ChannelMode m_channelMode = ChannelMode::SingleEnded;

    // some helper methods
    bool conversionComplete();

    // check that the input channel is valid for the current configuration
    void validateChannel(uint8_t channel);

    // methods for configuring the ADS1015
    void configSingleShot(uint16_t& config);
    void configPGA(uint16_t& config);
    void configChannel(uint16_t& config, uint8_t channel);
    void configSingleEndedChannel(uint16_t& config, uint8_t channel);
    void configDifferentialChannel(uint16_t& config, uint8_t channel);
    void configStartConversion(uint16_t& config);
};

#endif  // DEVCOM_ADS1015_H
