#ifndef PCA9548ACOM_H
#define PCA9548ACOM_H

#include <memory>

#include "I2CCom.h"

//! \brief PCA9548A Low Voltage 8-Channel I2C Switch with Reset
/**
 * [Datasheet](https://www.ti.com/lit/ds/symlink/pca9548a.pdf)
 *
 * Implements I2C communicatoin with a device that is connected to the
 * manager using the PCA9548A multiplexer. The I2C calls are forwarded
 * to right device using the internal `com` object as follows:
 *
 *  1. Enable only PCA9548A's `channel` output
 *  2. Change `com` target device address to `deviceAddr`
 *  3. Perform requested I2C operation using `com`
 *  4. Restore `com` target device address to PCA9548A
 *  5. Disable all PCA9548A's channels
 *
 * The implementation assumes that the state of the PCA9548A control
 * register can be changed externally (ie: in parallel process). This
 * is why it always enables the desired channel.
 *
 * All channels are disabled at the end in case of parallel PCA9548A
 * devices. This way only a single multiplexed I2C bus is available
 * for any communication request to any parallel PCA9548A device.
 *
 * The internal `com` object should target the PCA9548A device. The
 * device address should not be changed after creating the object, as
 * it is cached by the constructor.
 */
class PCA9548ACom : public I2CCom {
 public:
    /**
     * \param deviceAddr Target device address to which all I2C calls are
     * forwarded \param channel Output channel to which the device is connected
     * to \param com Internal `com` device targettign the PCA9548A.
     */
    PCA9548ACom(uint8_t deviceAddr, uint8_t channel,
                std::shared_ptr<I2CCom> com);
    virtual ~PCA9548ACom();

    /** Write commands @{ */

    virtual void write_reg32(uint32_t address, uint32_t data);
    virtual void write_reg16(uint32_t address, uint16_t data);
    virtual void write_reg8(uint32_t address, uint8_t data);

    virtual void write_reg32(uint32_t data);
    virtual void write_reg16(uint16_t data);
    virtual void write_reg8(uint8_t data);

    virtual void write_block(uint32_t address,
                             const std::vector<uint8_t>& data);
    virtual void write_block(const std::vector<uint8_t>& data);

    /** @} */

    /** Read commands @{ */

    virtual uint32_t read_reg32(uint32_t address);
    virtual uint32_t read_reg24(uint32_t address);
    virtual uint16_t read_reg16(uint32_t address);
    virtual uint8_t read_reg8(uint32_t address);

    virtual uint32_t read_reg32();
    virtual uint32_t read_reg24();
    virtual uint16_t read_reg16();
    virtual uint8_t read_reg8();

    virtual void read_block(uint32_t address, std::vector<uint8_t>& data);
    virtual void read_block(std::vector<uint8_t>& data);

    /** @} */

    /** Request exclusive access to device (if m_com is DevComuino)
     *
     * If a single hardware bus is used to connect multiple devices,
     * the access to all of them should be locked to remove changes
     * of cross-talk.
     *
     * Throw `std::runtime_error` on error.
     *
     */
    virtual void lock();

    /** Release exclusive access to device (if m_com is DevComuino)
     *
     * Throw `std::runtime_error` on error.
     *
     */
    virtual void unlock();

 private:
    std::shared_ptr<I2CCom> m_com;
    uint8_t m_muxAddr;
    uint8_t m_channel;

    //! Restore multiplexer communication into default state
    /**
     * Sets the `m_com` device address to the mux address
     * and disables all of the channels.
     */
    void restore_com();
};

#endif  // PCA9548ACOM_H
