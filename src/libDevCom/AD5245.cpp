#include "AD5245.h"

#include "LinearCalibration.h"
#include "OutOfRangeException.h"

AD5245::AD5245(double RAB, std::shared_ptr<I2CCom> com)
    : Potentiometer(std::make_shared<LinearCalibration>(RAB, 256, 100)),
      m_com(com) {}

void AD5245::writeCount(int32_t pos, uint8_t ch) {
    if (pos < 0 || 0xFF < pos) throw OutOfRangeException(pos, 0, 0xFF);

    if (ch != 0) throw OutOfRangeException(ch, 0, 0);

    m_com->write_reg16(0x0000 | pos);
}

int32_t AD5245::readCount(uint8_t ch) {
    if (ch != 0) throw OutOfRangeException(ch, 0, 0);

    return m_com->read_reg8();
}
