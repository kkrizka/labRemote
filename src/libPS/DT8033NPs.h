#ifndef DT8033NPS_H
#define DT8033NPS_H

#include <chrono>
#include <memory>
#include <string>

#include "IPowerSupply.h"

//! \brief Base implementation for the CAEN DT8033NPs power supplies
/**
 * CAEN DT8033NPs USB High Voltage Power Supplies.
 *
 *  Example configuration for a DT8033N PS:
 *
 *   "name": "myDT8033NPs",
 *   "hw-type": "PS",
 *   "hw-model": "DT8033NPs",
 *   "communication": {
 *      "protocol" : "TextSerialCom",
 *      "termination" : "\r\n",
 *      "baudrate" : "B9600",
 *      "port": "/dev/ttyACM1"
 *   }
 *
 * [Programming Manual](https://www.caen.it/products/dt8033/)
 */
class DT8033NPs : public IPowerSupply {
 public:
    /**
     * Interpretation of status bits.
     * The value is the bitmask to select the bit in status() return value.
     */
    enum Status {
        On = (1 << 0),           // 1 : ON 0 : OFF
        RampingUp = (1 << 1),    // 1 : Channel Ramping UP
        RampingDown = (1 << 2),  // 1 : Channel Ramping DOWN
        OVC = (1 << 3),          // 1 : Over current
        OVV = (1 << 4),          // 1 : Over voltage
        UNV = (1 << 5),          // 1 : Under voltage
        MAXV = (1 << 6),         // 1 : VOUT in MAXV protection
        Trip = (1 << 7),         // 1 : Current generator
        OVT = (1 << 8),          // 1 : Over temperature
        Disabled = (1 << 10),    // 1 : Ch disabled
        Kill = (1 << 11),        // 1 : Ch in KILL
        Interlock = (1 << 12),   // 1 : Ch in INTERLOCK
        CalError = (1 << 13)     // 1 : Calibration Error
    };

    /**
     * Polarity of the power supply output
     */
    enum Polarity { Positive, Negative };

    /**
     * Range of the current monitor
     */
    enum IMonRange { High, Low };

    //! \brief Constructor that configures checks for a specific collection of
    //! models
    /**
     * @param name `IPowerSupply` instance name
     * @param models List of supported model names (see
     * `IPowerSupply::checkCompatibilityList`)
     * @param output The output voltage is negative
     * @param imaxl Maximum current [A] for low IMon range
     */
    DT8033NPs(const std::string& name);
    ~DT8033NPs() = default;

    /** \name Communication
     * @{
     */

    virtual bool ping();

    virtual std::string identify();

    /**
     * In addition to the standard model check from `IPowerSupply`, this also
     * checks that the `output` setting is correct.
     */
    virtual void checkCompatibilityList();

    /** @} */

    /** \name Power Supply Control
     * @{
     */

    virtual void reset();

    /** \brief Turn on power supply
     *
     * Block until power supply finishes ramping.
     *
     * @param channel channel, if any
     */
    virtual void turnOn(unsigned channel);

    /** \brief Turn off power supply
     *
     * Block until power supply finishes rampdown.
     * @param channel channel, if any
     */
    virtual void turnOff(unsigned channel);

    /** @} */

    /** \name Current Control and Measurement
     * @{
     */

    /**
     * Calls `setCurrentProtect` with `cur` as the maximum level.
     */
    virtual void setCurrentLevel(double cur, unsigned channel = 1);
    virtual double getCurrentLevel(unsigned channel = 1);

    //! \brief Set current protection
    /**
     * The current monitor range is also automatically set to match up
     * with the maximum current (`maxcur`). If `maxcur` is below the maximum
     * of the low IMon range (`imaxl`, see constructor), then low Imon range
     * is used. Otherwise the high IMon range is used.
     *
     * @param maxcur maximum current (absolute value) [A]
     * @param channel channel (if any)
     */
    virtual void setCurrentProtect(double maxcur, unsigned channel = 1);

    //! \brief Get current protection
    /**
     * @param channel channel (if any)
     * @return maximum current (absolute value) [A]
     */
    virtual double getCurrentProtect(unsigned channel = 1);
    virtual double measureCurrent(unsigned channel = 1);

    /** @} */

    /** \name Voltage Control and Measurement
     * @{
     */

    //! \brief Set output voltage level.
    /**
     * For the N-type power supplies, the voltage level should be
     * specified as negative. This function uses the `output`
     * property to validate the input.
     *
     * @param volt voltage [V]
     * @param channel channel (if any)
     */
    virtual void setVoltageLevel(double volt, unsigned channel = 1);
    virtual double getVoltageLevel(unsigned channel = 1);

    //! \brief Set voltage protection
    /**
     * @param maxvolt maximum voltage (absolute value) [V]
     * @param channel channel (if any)
     */
    virtual void setVoltageProtect(double maxvolt, unsigned channel = 1);

    //! \brief Get voltage protection
    /**
     * @param channel channel (if any)
     * @return maximum voltage (absolute value) [V]
     */
    virtual double getVoltageProtect(unsigned channel = 1);

    virtual double measureVoltage(unsigned channel = 1);

    /** @} */

    /** \name Model-specific functionality
     * @{
     */

    /**
     * Return the status of the Power Supply.
     * Use with Status enum to interpret bits.
     *
     * @param channel Channel to query
     *
     * @return status bits
     */
    uint16_t status(unsigned channel = 1);

    /**
     * Return the polarity of the power supply.
     *
     * @param channel Channel to query
     *
     * @return polarity of the channel
     */
    Polarity polarity(unsigned channel = 1);

    /**
     * Set the current monitoring range
     *
     * @param range to set
     */
    void setIMonRange(IMonRange range = Low, unsigned channel = 1);

    /**
     * Get the current monitoring range
     *
     * @return currently set range
     */
    IMonRange getIMonRange(unsigned channel = 1);

    //! \brief Wait for voltage ramp to complete
    /**
     * Monitors the status of the power supply every
     * second until the ramp up and down bits are zero.
     *
     * The monitoring starts by waiting for 1 second for
     * the status register to be updated.
     */
    void waitRamp(unsigned channel = 1);

    /** @} */

 private:
    //! \brief Build a command string and parse the response
    /**
     * Throws an exception if any of the following errors are detected:
     * - no response
     * - returned CMD value is ERR
     *
     * @param cmd CMD value
     * @param par PAR value
     * @param value VAL value (if empty, not appended)
     *
     * @return The returned VAL value.
     */
    std::string command(const std::string& cmd, const std::string& par,
                        unsigned channel = 99, const std::string& value = "");

    //! \brief Check that the input (voltage or current) has the right sign and
    //! convert to absolute
    /**
     * A `std::runtime_error` is thrown if a wrong sign is supplied, as
     * determine by the `m_output` setting.
     *
     * The conversion to absolute value is necessary for the power supplies
     * command protocol.
     *
     * @param `input` Input value to check and convert.
     *
     * @return Absolute value of `input`
     */
    double checkPolarity(double input);

    //! \brief Specify whether power supply outputs a negative voltage (N vs P
    //! model)
    Polarity m_output = Polarity::Negative;
};

#endif  // DT8033NPS_H
