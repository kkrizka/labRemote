#include "DT8033NPs.h"

#include <algorithm>
#include <thread>

#include "Logger.h"
#include "ScopeLock.h"
#include "TextSerialCom.h"

// Register power supply
#include "PowerSupplyRegistry.h"

REGISTER_POWERSUPPLY(DT8033NPs)

DT8033NPs::DT8033NPs(const std::string& name)
    : IPowerSupply(name, {"DT8033"}) {}

void DT8033NPs::reset() {
    command("SET", "OFF");
    command("SET", "BDCLR");

    std::string result = identify();
    if (result.empty())
        throw std::runtime_error("No communication after reset.");
}

std::string DT8033NPs::identify() {
    command("SET", "BDCLR");
    std::string idn = command("MON", "BDNAME");
    return idn;
}

bool DT8033NPs::ping() {
    std::string result = command("MON", "BDNAME");
    return !result.empty();
}

void DT8033NPs::checkCompatibilityList() {
    IPowerSupply::checkCompatibilityList();
}

void DT8033NPs::turnOn(unsigned channel) {
    ScopeLock lock(m_com);
    command("SET", "ON", channel);
    waitRamp();
}

void DT8033NPs::turnOff(unsigned channel) {
    ScopeLock lock(m_com);
    command("SET", "OFF", channel);
    waitRamp();
}

void DT8033NPs::setCurrentLevel(double cur, unsigned channel) {
    command("SET", "ISET", channel, std::to_string(cur));
}

double DT8033NPs::getCurrentLevel(unsigned channel) {
    return std::stod(command("MON", "ISET", channel)) / 1e6;
}

void DT8033NPs::setCurrentProtect(double maxcur, unsigned channel) {
    setCurrentLevel(maxcur, channel);
}

double DT8033NPs::getCurrentProtect(unsigned channel) {
    return std::fabs(getCurrentLevel(channel));
}

double DT8033NPs::measureCurrent(unsigned channel) {
    return std::stod(command("MON", "IMON", channel)) / 1e6;
}

void DT8033NPs::setVoltageLevel(double volt, unsigned channel) {
    command("SET", "VSET", channel, std::to_string(checkPolarity(volt)));
    waitRamp();
}

double DT8033NPs::getVoltageLevel(unsigned channel) {
    return std::stod(command("MON", "VSET", channel));
}

void DT8033NPs::setVoltageProtect(double maxvolt, unsigned channel) {
    setVoltageLevel(maxvolt, channel);
}

double DT8033NPs::getVoltageProtect(unsigned channel) {
    return std::fabs(getVoltageLevel(channel));
}

double DT8033NPs::measureVoltage(unsigned channel) {
    return std::stod(command("MON", "VMON", channel));
}

uint16_t DT8033NPs::status(unsigned channel) {
    return std::stoi(command("MON", "STAT", channel)) & 0xFFFF;
}

DT8033NPs::Polarity DT8033NPs::polarity(unsigned channel) {
    std::string polstr = command("MON", "POLARITY", channel);
    if (polstr == "-")
        return Polarity::Negative;
    else
        return Polarity::Positive;
}

void DT8033NPs::setIMonRange(IMonRange range, unsigned channel) {
    command("SET", "IMRANGE", channel,
            (range == IMonRange::Low) ? "LOW" : "HIGH");
}

DT8033NPs::IMonRange DT8033NPs::getIMonRange(unsigned channel) {
    return (command("MON", "IMRANGE", channel) == "LOW") ? IMonRange::Low
                                                         : IMonRange::High;
}

void DT8033NPs::waitRamp(unsigned channel) {
    do {
        std::this_thread::sleep_for(std::chrono::seconds(1));
        logger(logDEBUG) << __PRETTY_FUNCTION__
                         << " -> ramping: " << measureVoltage(channel) << "V";
    } while (status(channel) & (Status::RampingUp | Status::RampingDown));
}

std::string DT8033NPs::command(const std::string& cmd, const std::string& par,
                               unsigned channel, const std::string& value) {
    // Build command
    std::string tosend = "$CMD:" + cmd;
    if (channel != 99) tosend += ",CH:" + std::to_string(channel);
    tosend += ",PAR:" + par;
    if (!value.empty()) tosend += ",VAL:" + value;

    // Send command and receive response
    std::string resp = m_com->sendreceive(tosend);

    // Parse response
    if (resp.empty()) throw "DT8033: No response :(";

    std::string retvalue;
    std::string cmdvalue;

    std::string token;
    std::stringstream ss(resp);
    while (std::getline(ss, token, ',')) {
        size_t seppos = token.find(':');
        if (seppos == std::string::npos) continue;  // Not a valid part
        if (token.substr(0, seppos) == "VAL") {     // This is the value part!
            retvalue = token.substr(seppos + 1);
        } else if (token.substr(0, seppos) ==
                   "#CMD") {  // This is the value part!
            cmdvalue = token.substr(seppos + 1);
        }
    }

    if (cmdvalue.empty())
        throw std::runtime_error("DT8033: No CMD in return statement :(");

    if (cmdvalue == "ERR")
        throw std::runtime_error("DT8033: CMD shows an error :(");

    return retvalue;
}

double DT8033NPs::checkPolarity(double input) {
    if (m_output == Polarity::Negative && input > 0)
        throw std::runtime_error(
            "Specified positive output value for a power supply that only "
            "supports negative output.");
    if (m_output == Polarity::Positive && input < 0)
        throw std::runtime_error(
            "Specified negative output value for a power supply that only "
            "supports positive output.");

    return std::fabs(input);
}
