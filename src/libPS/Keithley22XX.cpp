#include "Keithley22XX.h"

#include <algorithm>
#include <thread>

#include "Logger.h"
#include "ScopeLock.h"

// Register power supply
#include "PowerSupplyRegistry.h"
REGISTER_POWERSUPPLY(Keithley22XX)

Keithley22XX::Keithley22XX(const std::string& name)
    : IPowerSupply(name, {"2231A"}) {}

bool Keithley22XX::ping() {
    std::string result = m_com->sendreceive("*IDN?");
    return !result.empty();
}

void Keithley22XX::reset() {
    m_com->send("OUTP:ALL 0");
    m_com->send("*RST");

    if (!ping()) throw std::runtime_error("No communication after reset.");
}

std::string Keithley22XX::identify() {
    std::string idn = m_com->sendreceive("*IDN?");
    return idn;
}

void Keithley22XX::turnOn(unsigned channel) {
    if (channel != 1 && channel != 2 && channel != 3)
        throw std::runtime_error("Invalid channel: " + std::to_string(channel));

    ScopeLock lock(m_com);
    m_com->send("INST CH" + std::to_string(channel));
    m_com->send("CHAN:OUTP 1");
}

void Keithley22XX::turnOff(unsigned channel) {
    if (channel != 1 && channel != 2 && channel != 3)
        throw std::runtime_error("Invalid channel: " + std::to_string(channel));

    ScopeLock lock(m_com);
    m_com->send("INST CH" + std::to_string(channel));
    m_com->send("CHAN:OUTP 0");
}

bool Keithley22XX::isOn(unsigned channel) {
    if (channel != 1)
        throw std::runtime_error(
            "Set the channel to 1 for single channel power-supply");

    return m_com->sendreceive(":OUTPUT?") == "1";
}

void Keithley22XX::setCurrentLevel(double cur, unsigned channel) {
    if (channel != 1 && channel != 2 && channel != 3)
        throw std::runtime_error("Invalid channel: " + std::to_string(channel));

    ScopeLock lock(m_com);
    m_com->send("INST CH" + std::to_string(channel));
    m_com->send(":SOURCE:FUNC CURR");
    m_com->send(":SOURCE:CURR " + std::to_string(cur));
}

double Keithley22XX::getCurrentLevel(unsigned channel) {
    if (channel != 1 && channel != 2 && channel != 3)
        throw std::runtime_error("Invalid channel: " + std::to_string(channel));

    ScopeLock lock(m_com);
    m_com->send("INST CH" + std::to_string(channel));
    return std::stod(m_com->sendreceive(":SOURCE:CURR?"));
}

void Keithley22XX::setCurrentProtect(double maxcur, unsigned channel) {
    if (channel != 1 && channel != 2 && channel != 3)
        throw std::runtime_error("Invalid channel: " + std::to_string(channel));

    ScopeLock lock(m_com);
    m_com->send("INST CH" + std::to_string(channel));
    m_com->send(":SENSE:CURR:PROTECTION " + std::to_string(maxcur));
}

double Keithley22XX::getCurrentProtect(unsigned channel) {
    if (channel != 1 && channel != 2 && channel != 3)
        throw std::runtime_error("Invalid channel: " + std::to_string(channel));

    ScopeLock lock(m_com);
    m_com->send("INST CH" + std::to_string(channel));
    return std::stod(m_com->sendreceive(":SENSE:CURR:PROTECTION?"));
}

double Keithley22XX::measureCurrent(unsigned channel) {
    if (channel != 1 && channel != 2 && channel != 3)
        throw std::runtime_error("Invalid channel: " + std::to_string(channel));

    if (!isOn(channel)) return 0.;

    ScopeLock lock(m_com);
    m_com->send(":FORMAT:ELEMENTS CURR");
    m_com->send("INST CH" + std::to_string(channel));

    return std::stod(m_com->sendreceive(":READ?"));
}

void Keithley22XX::setVoltageLevel(double volt, unsigned channel) {
    if (channel != 1 && channel != 2 && channel != 3)
        throw std::runtime_error("Invalid channel: " + std::to_string(channel));

    ScopeLock lock(m_com);
    m_com->send("INST CH" + std::to_string(channel));
    m_com->send(":SOURCE:FUNC VOLT");
    m_com->send(":SOURCE:VOLT " + std::to_string(volt));
}

double Keithley22XX::getVoltageLevel(unsigned channel) {
    if (channel != 1 && channel != 2 && channel != 3)
        throw std::runtime_error("Invalid channel: " + std::to_string(channel));

    ScopeLock lock(m_com);
    m_com->send("INST CH" + std::to_string(channel));
    return std::stod(m_com->sendreceive(":SOURCE:VOLT?"));
}

void Keithley22XX::setVoltageProtect(double maxvolt, unsigned channel) {
    if (channel != 1 && channel != 2 && channel != 3)
        throw std::runtime_error("Invalid channel: " + std::to_string(channel));

    ScopeLock lock(m_com);
    m_com->send("INST CH" + std::to_string(channel));
    m_com->send(":SENSE:VOLT:PROTECTION " + std::to_string(maxvolt));
}

double Keithley22XX::getVoltageProtect(unsigned channel) {
    if (channel != 1 && channel != 2 && channel != 3)
        throw std::runtime_error("Invalid channel: " + std::to_string(channel));

    ScopeLock lock(m_com);
    m_com->send("INST CH" + std::to_string(channel));
    return std::stod(m_com->sendreceive(":SENSE:VOLT:PROTECTION?"));
}

double Keithley22XX::measureVoltage(unsigned channel) {
    if (channel != 1 && channel != 2 && channel != 3)
        throw std::runtime_error("Invalid channel: " + std::to_string(channel));

    ScopeLock lock(m_com);

    if (!isOn(channel)) return 0.;

    m_com->send(":FORMAT:ELEMENTS VOLT");
    m_com->send("INST CH" + std::to_string(channel));

    return std::stod(m_com->sendreceive(":READ?"));
}
