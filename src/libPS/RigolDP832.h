#ifndef RIGOLDP832_H
#define RIGOLDP832_H

#include <chrono>
#include <memory>
#include <string>

#include "SCPIPs.h"
#include "SerialCom.h"

/** \brief Rigol DP832
 *
 * Implementation for the Rigol DP832 power supply.
 *
 * [Programming
 * Manual](http://beyondmeasure.rigoltech.com/acton/attachment/1579/f-03a1/1/-/-/-/-/DP800%20Programming%20Guide.pdf)
 *
 * Other Rigol DP8xx power supplies might be supported too, but
 * have not been checked.
 */
class RigolDP832 : public SCPIPs {
 public:
    RigolDP832(const std::string& name);
    ~RigolDP832() = default;
};

#endif
