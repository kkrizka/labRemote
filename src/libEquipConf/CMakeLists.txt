#
# Prepare the library

# Add all *.c* files as source code
set(SrcFiles
  EquipConf.cpp
  DataSinkConf.cpp)

# create target library
add_library(EquipConf SHARED ${SrcFiles})

target_link_libraries(EquipConf PUBLIC Utils PS Com DataSink Chiller Meter ${JSON_LIBS} nlohmann_json_schema_validator)
target_include_directories(EquipConf PUBLIC ${CMAKE_CURRENT_SOURCE_DIR})

# Tell rest of labRemote that the library exists
set(libEquipConf_FOUND TRUE PARENT_SCOPE)

set_target_properties(EquipConf PROPERTIES VERSION ${labRemote_VERSION_MAJOR}.${labRemote_VERSION_MINOR})
install(TARGETS EquipConf)
