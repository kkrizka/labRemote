#ifndef ILOCKABLE_H
#define ILOCKABLE_H

//! Abstract interface for classes that support multi-process access to a device
/**
 * Calling `lock` gives user exclusive access to the device represented by the
 * class across multiple processes. Calling `unlock` releases that access.
 *
 * Multiple calls to `lock` should increment a counter, which is then
 * decremented by a call to `unlock`. Only once the counter has reached 0 should
 * the access be released.
 */
class ILockable {
 public:
    virtual void lock() = 0;
    virtual void unlock() = 0;
};

#endif  // ILOCKABLE_H
