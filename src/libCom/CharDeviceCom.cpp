#include "CharDeviceCom.h"

#include <fcntl.h>
#include <sys/file.h>
#include <unistd.h>

#include <cerrno>
#include <cstring>
#include <stdexcept>

#include "Logger.h"
#include "ScopeLock.h"

// Register com
#include "ComRegistry.h"
REGISTER_COM(CharDeviceCom)

CharDeviceCom::CharDeviceCom(const std::string &port) : m_port(port) {}

CharDeviceCom::CharDeviceCom() {}

CharDeviceCom::~CharDeviceCom() {
    if (m_dev) close(m_dev);
}

void CharDeviceCom::init() {
    if (is_open()) return;

    if (m_port.empty())
        throw std::runtime_error("CharDeviceCom::init requires a device name");

    m_dev = open(m_port.c_str(), O_RDWR | O_NOCTTY);
    m_good = true;
}

void CharDeviceCom::send(const std::string &buf) {
    ScopeLock lock(this);
    int n_write = ::write(m_dev, buf.c_str(), buf.size());

    if (n_write < 0)
        throw std::runtime_error("Error writing to " + m_port + ": " +
                                 std::strerror(errno));
}

void CharDeviceCom::send(char *buf, size_t length) {
    ScopeLock lock(this);
    int n_write = ::write(m_dev, buf, length);

    if (n_write < 0)
        throw std::runtime_error("Error writing to " + m_port + ": " +
                                 std::strerror(errno));
}

std::string CharDeviceCom::receive() {
    ScopeLock lock(this);
    int n_read = ::read(m_dev, m_tmpbuf, MAX_READ);

    if (n_read >= 0)
        return std::string(m_tmpbuf, n_read);
    else
        throw std::runtime_error("Error reading from " + m_port + ": " +
                                 std::strerror(errno));
}

uint32_t CharDeviceCom::receive(char *buf, size_t length) {
    ScopeLock lock(this);
    int n_read = ::read(m_dev, buf, length);

    if (n_read < 0)
        throw std::runtime_error("Error reading from " + m_port + ": " +
                                 std::strerror(errno));

    return n_read;
}

std::string CharDeviceCom::sendreceive(const std::string &cmd) {
    ScopeLock lock(this);

    send(cmd);
    std::string ret = receive();

    return ret;
}

void CharDeviceCom::sendreceive(char *wbuf, size_t wlength, char *rbuf,
                                size_t rlength) {
    ScopeLock lock(this);

    send(wbuf, wlength);
    receive(rbuf, rlength);
}

void CharDeviceCom::lock() {
    flock(m_dev, LOCK_EX);
    m_lock_counter++;
}

void CharDeviceCom::unlock() {
    if (m_lock_counter == 0) return;  // No lock exists

    m_lock_counter--;
    if (m_lock_counter == 0) flock(m_dev, LOCK_UN);
}

void CharDeviceCom::setConfiguration(const nlohmann::json &config) {
    for (const auto &kv : config.items()) {
        if (kv.key() == "port") {
            m_port = kv.value();
        }
    }
}
