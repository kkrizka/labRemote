#include "LXICom.h"

#include "Logger.h"

/*
 */
#include <stdio.h>
#include <string.h>
#include <unistd.h>

#include <cerrno>
#include <cstring>
#include <stdexcept>

// tcp net
#include <netdb.h>        //hostent
#include <netinet/tcp.h>  // linux extension: socket options (timeout)
#include <sys/socket.h>

// Register com
#include "ComRegistry.h"
REGISTER_COM(LXICom)

LXICom::LXICom(const std::string &host, const uint16_t port)
    : m_host(host), m_port(port) {}

LXICom::~LXICom() { tcp_disconnect(); }

#define TCP_MESAGE_MAX_SIZE 1024  // used for all messages
#define IP_STR 32

#define STRING_FULLADDR (m_host + ":" + std::to_string(m_port))

/** Connect the TCP socket */
int LXICom::tcp_connect() {
    // Create socket
    m_socket_desc = socket(AF_INET, SOCK_STREAM, 0);
    if (m_socket_desc == -1) {
        throw std::runtime_error("LXICom::tcp_connect could not create socket");
    }

    // timeout settings (linux-only)
    const int synRetries = 1;  // Send a total of 3 SYN packets => Timeout ~7s
    setsockopt(m_socket_desc, IPPROTO_TCP, TCP_SYNCNT, &synRetries,
               sizeof(synRetries));

    // Connect
    if (connect(m_socket_desc, (struct sockaddr *)&m_server_addr,
                sizeof(m_server_addr)) < 0) {
        throw std::runtime_error("LXICom::tcp_connect connect error " +
                                 STRING_FULLADDR + " " +
                                 std::string(strerror(errno)));
    }
    logger(logDEBUG) << "LXICom::tcp_connect connected";

    return 0;
}

int LXICom::tcp_disconnect() {
    close(m_socket_desc);
    logger(logDEBUG) << "LXICom::tcp_disconnect disconnected";
    return 0;
}

/** Send a message to the TCP socket */
int LXICom::tcp_send(const char *message) {
    // Send the message
    if (::send(m_socket_desc, message, strlen(message), 0) < 0) {
        throw std::runtime_error("LXICom::tcp_send send failed " +
                                 STRING_FULLADDR + " " +
                                 std::string(strerror(errno)));
    }
    logger(logDEBUG) << "LXICom::tcp_send sent " << message;

    return 0;
}

/** Receive a message on the TCP socket */
int LXICom::tcp_recv(std::string &response) {
    char tmp_buf[TCP_MESAGE_MAX_SIZE] = {0};
    // Receive only 1 reply
    ssize_t response_len = recv(m_socket_desc, tmp_buf, TCP_MESAGE_MAX_SIZE, 0);

    if (response_len < 0) {
        throw std::runtime_error("LXICom::tcp_recv recv failed " +
                                 STRING_FULLADDR + " " +
                                 std::string(strerror(errno)));
    } else if (response_len == 0) {
        // the device closed the connection
        // which does not mean that it won't accept new messages
        // but it does mean that the response is over
        // re-establish the connection
        logger(logDEBUG) << "LXICom::tcp_recv device closed the connection, "
                            "re-establish the connection";
        tcp_connect();
    }
    logger(logDEBUG) << "LXICom::tcp_recv recv " << tmp_buf << " len "
                     << response_len;

    response.assign(tmp_buf);
    return 0;
}

/** resolve hostname to ip address string */
int resolve_hostname(const char hostname[], char ip_str[]) {
    struct hostent *he;
    struct in_addr **addr_list;

    if ((he = gethostbyname(hostname)) == NULL) {
        // gethostbyname failed
        throw std::runtime_error("resolve_hostname gethostbyname failed: " +
                                 std::string(hostname));
    }

    // Cast the h_addr_list to in_addr, since h_addr_list also has the ip
    // address in long format only
    addr_list = (struct in_addr **)he->h_addr_list;

    for (int i = 0; addr_list[i] != NULL; i++) {
        // Return the first one;
        logger(logDEBUG) << "resolve_hostname inet_ntoa " << i << " "
                         << inet_ntoa(*addr_list[i]);
        strncpy(ip_str, inet_ntoa(*addr_list[i]), IP_STR);
    }

    return 0;
}

void LXICom::init() {
    if (is_open()) return;
    logger(logDEBUG) << "LXICom::init m_host=" << m_host
                     << " m_port=" << m_port;

    if (m_host.empty())
        throw std::runtime_error(
            "LXICom::init requires a host:port address pair of the device on "
            "LXI");

    // resolve the hostname
    char ip_str[IP_STR];
    resolve_hostname(m_host.c_str(), ip_str);
    logger(logDEBUG) << "LXICom::init resolved " << m_host << " to " << ip_str;

    // set the server address structure
    m_server_addr.sin_addr.s_addr = inet_addr(ip_str);
    m_server_addr.sin_family = AF_INET;
    m_server_addr.sin_port = htons(m_port);

    // test connection
    tcp_connect();
    logger(logDEBUG) << "LXICom::init connection to " << STRING_FULLADDR
                     << " established";

    m_good = true;
}

void LXICom::send(const std::string &buf) {
    logger(logDEBUG) << "LXICom::send " << buf;
    tcp_send(buf.c_str());
}

void LXICom::send(char *buf, size_t length) { send(std::string(buf, length)); }

std::string LXICom::receive() {
    tcp_recv(m_receive_buf);

    if (m_receive_buf.empty())
        throw std::runtime_error(
            "LXICom::receive error: nothing is received  from " + m_host + ":" +
            std::to_string(m_port));

    logger(logDEBUG) << "LXICom::receive " << m_receive_buf;
    return m_receive_buf;
}

uint32_t LXICom::receive(char *buf, size_t length) {
    std::string received = receive();

    strncpy(buf, received.c_str(), length);
    return (received.size() < length ? received.size() : length);
}

std::string LXICom::sendreceive(const std::string &cmd) {
    send(cmd);
    std::string ret = receive();

    return ret;
}

void LXICom::sendreceive(char *wbuf, size_t wlength, char *rbuf,
                         size_t rlength) {
    send(wbuf, wlength);
    receive(rbuf, rlength);
}

void LXICom::lock() {
    logger(logWARNING)
        << "No   locking is implemented for LXI communication for device "
        << m_host;
}

void LXICom::unlock() {
    logger(logWARNING)
        << "No unlocking is implemented for LXI communication for device "
        << m_host;
}

void LXICom::setConfiguration(const nlohmann::json &config) {
    for (const auto &kv : config.items()) {
        if (kv.key() == "host") {
            m_host = kv.value();
        }
        if (kv.key() == "port") {
            m_port = kv.value();
        }
    }
}
