#include "ComRegistry.h"

#include <iostream>

namespace EquipRegistry {

typedef ClassRegistry<ICom> RegistryCom;

static RegistryCom& registry() {
    static RegistryCom instance;
    return instance;
}

bool registerCom(const std::string& model,
                 std::function<std::shared_ptr<ICom>()> f) {
    return registry().registerClass(model, f);
}

std::shared_ptr<ICom> createCom(const std::string& model) {
    auto result = registry().makeClass(model);
    if (result == nullptr)
        std::cout << "No communication (ICom) of type " << model << " found\n";

    return result;
}

std::vector<std::string> listCom() { return registry().listClasses(); }

}  // namespace EquipRegistry
