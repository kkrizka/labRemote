#ifndef HUBERUNICHILLER010_H
#define HUBERUNICHILLER010_H

#include <memory>
#include <stdexcept>
#include <string>

#include "IChiller.h"
#include "Logger.h"
#include "TextSerialCom.h"

//! \brief Object to interface with a Huber Unichiller or Minichiller series
//! chiller
/**
 * The TextSerialCom passed to this object should already be
 * initialized, be set to the same baud rate as the chiller,
 * and have the termination set to "\r\n".
 *
 * # Example
 *
 * ```
 * std::shared_ptr<TextSerialCom> com =
 *   std::make_shared<TextSerialCom>("/dev/ttyUSB0",B9600);
 * com->setTermination("\r\n");
 * com->init();
 *
 * HuberChiller chiller;
 *
 * chiller.setCom(com);
 * chiller.init();
 * chiller.setSetTemperature(15.0);
 * ```
 *
 * The operator's manual for these chillers can be found at
 * https://www.huber-online.com/en/download_manuals.aspx
 *
 * Specifically, the current operating manual is found at
 * https://www.huber-online.com/download/manuals/BAL%20OL%C3%89%20Minichiller%20Unichiller%20EN.pdf
 */
class HuberChiller : public IChiller {
 public:
    /**
     * \param com The serial communication interface connected
     *   to the chiller
     */
    HuberChiller(const std::string& name);
    ~HuberChiller() = default;

    //! Initialize the serial communication channel
    void init();
    /**
     *
     * The object is initialized if not already.
     *
     * \param com TextSerialCom instance for communicating with the chiller
     */
    void setCom(std::shared_ptr<ICom> com);
    //! Turn the chiller on
    void turnOn();
    //! Turn the chiller off
    void turnOff();
    //! Set the target temperature of the chiller
    /**
     * \param temp The target temperature in Celsius
     */
    void setTargetTemperature(float temp);
    //! Return the temperature that the chiller is set to in Celsius
    float getTargetTemperature();
    //! Return the current temperature of the chiller in Celsius
    float measureTemperature();
    //! Get the status of the chiller
    /**
     * \return true if chiller is in "run" mode, and false if
     *   it's in "standby" mode
     */
    bool getStatus();
    //! Returns error fault codes
    /**
     * \return "" if system OK, and an error code otherwise
     */
    std::string getFaultStatus();

 protected:
    std::shared_ptr<TextSerialCom> m_com;

 private:
    //! Hold the information about the chiller identification
    std::string m_identity = "";
    //! Send a command to the chiller
    std::string command(const std::string& cmd);
};

#endif
