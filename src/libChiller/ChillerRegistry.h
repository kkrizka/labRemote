#ifndef CHILLER_REGISTRY_H
#define CHILLER_REGISTRY_H

#include <map>
#include <string>
#include <vector>

#include "ClassRegistry.h"
#include "IChiller.h"

namespace EquipRegistry {

bool registerChiller(
    const std::string& model,
    std::function<std::shared_ptr<IChiller>(const std::string&)> f);

std::shared_ptr<IChiller> createChiller(const std::string& model,
                                        const std::string& name);

std::vector<std::string> listChiller();
}  // namespace EquipRegistry

#define REGISTER_CHILLER(model)                                               \
    static bool _registered_##model = EquipRegistry::registerChiller(         \
        #model, std::function<std::shared_ptr<IChiller>(const std::string&)>( \
                    [](const std::string& name) {                             \
                        return std::shared_ptr<IChiller>(new model(name));    \
                    }));

#endif  // CHILLER_REGISTRY_H
