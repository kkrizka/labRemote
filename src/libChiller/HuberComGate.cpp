#include "HuberComGate.h"

#include "ChillerRegistry.h"
REGISTER_CHILLER(HuberComGate)

#include <chrono>
#include <iomanip>
#include <thread>

HuberComGate::HuberComGate(const std::string& name)
    : IChiller(name, {"HuberComGate"}) {
    // nothing to be done
}

void HuberComGate::init() {
    // nothing to be done
}
void HuberComGate::turnOn() {
    m_com->send("START");
    std::this_thread::sleep_for(std::chrono::milliseconds(500));
}
void HuberComGate::turnOff() {
    m_com->send("STOP");
    std::this_thread::sleep_for(std::chrono::milliseconds(500));
}
void HuberComGate::setTargetTemperature(float temp) {
    std::stringstream cmd;
    cmd << "OUT_SP_00 " << std::fixed << std::setprecision(2) << temp;
    m_com->send(cmd.str());
    std::this_thread::sleep_for(std::chrono::milliseconds(500));
}
float HuberComGate::getTargetTemperature() {
    std::string response = m_com->sendreceive("IN_SP_00");
    std::this_thread::sleep_for(std::chrono::milliseconds(500));
    return std::stof(response);
}
float HuberComGate::measureTemperature() {
    std::string response = m_com->sendreceive("IN_PV_00");
    std::this_thread::sleep_for(std::chrono::milliseconds(500));
    return std::stof(response);
}
bool HuberComGate::getStatus() {
    std::string response = m_com->sendreceive("STATUS");
    std::this_thread::sleep_for(std::chrono::milliseconds(500));
    if (response == "-1") return false;  // only error state the device has
    return true;
}
