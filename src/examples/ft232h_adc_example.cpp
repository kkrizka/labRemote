// std/stl
#include <stdint.h>

#include <memory>  // shared_ptr, unique_ptr

// labremote
#include "AD799X.h"
#include "Logger.h"
#ifdef ENABLE_FTDI
#include "FT232H.h"
#include "I2CFTDICom.h"
#endif

#include "DeviceComRegistry.h"

int main(int argc, char* argv[]) {
#ifndef ENABLE_FTDI
    logger(logERROR) << "FTDI support is not enabled.";
    return 1;
#else

    // this is our I2C bus primary -- on the FT232H device -- that will control
    // communication with our I2C secondaries (the ADCs)
    std::shared_ptr<FT232H> ft232;
    try {
        ft232 = std::make_shared<FT232H>(MPSSEChip::Protocol::I2C,
                                         MPSSEChip::Speed::FOUR_HUNDRED_KHZ,
                                         MPSSEChip::Endianness::MSBFirst);
        logger(logINFO) << "Initialized MPSSE: " << ft232->to_string();
    } catch (std::exception& e) {
        logger(logERROR) << "Failed to initialized MPSSE: " << e.what();
        return 1;
    }

    // setup control of I2C secondary: ADC0 at bus address 0x21
    std::shared_ptr<I2CFTDICom> com0(new I2CFTDICom(ft232, 0x21));
    std::unique_ptr<AD799X> adc0(new AD799X(2.5, AD799X::Model::AD7998, com0));

    // setup control of I2C secondary: ADC1 at bus address 0x22
    std::shared_ptr<I2CFTDICom> com1(new I2CFTDICom(ft232, 0x22));
    std::unique_ptr<AD799X> adc1(new AD799X(2.5, AD799X::Model::AD7998, com1));

    uint8_t vddd_chan = 0;  // Channel 0 on ADC0 samples VDDD
    uint8_t ntc_chan = 6;   // Channel 6 on ADC1 amples the NTC

    logger(logINFO) << "------------------";
    for (size_t i = 0; i < 10; i++) {
        logger(logINFO) << "[" << i << "] VDD = " << adc0->read(vddd_chan)
                        << ", NTC = " << adc1->read(ntc_chan);
    }
    logger(logINFO) << "------------------";

    return 0;
#endif  // FTDI
}
