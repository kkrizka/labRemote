#include <iomanip>
#include <iostream>

#include "Fluke8842.h"
#include "GPIBSerialCom.h"
#include "Logger.h"
void usage(char* argv[]) {
    // example usage
    std::cerr << "Usage: " << argv[0] << " USB port "
              << " GPIB address" << std::endl;
    std::cerr << "An example \n ./examples/Fluke8842_example /dev/ttyUSB2 3"
              << std::endl;
}

int main(int argc, char* argv[]) {
    if (argc < 3) {
        usage(argv);
        return 1;
    }

    Fluke8842* meter = new Fluke8842("Fluke 8842");

    long gpib_addr = std::atoi(argv[2]);
    std::shared_ptr<GPIBSerialCom> com = std::make_shared<GPIBSerialCom>(
        gpib_addr, argv[1], SerialCom::BaudRate::Baud115200);
    // Set the communication protocol to the device
    meter->setCom(com);

    logger(logINFO) << "The multimeter IDN is: " + meter->identify();

    logger(logINFO) << meter->GetMode();
    logger(logINFO) << meter->measureDCI();

    logger(logINFO) << meter->GetMode();
    logger(logINFO) << meter->measureDCV();

    logger(logINFO) << meter->GetValue();

    return 0;
}
